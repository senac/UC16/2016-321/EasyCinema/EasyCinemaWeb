package br.com.senac.EasyCinemaWeb.banco;

import br.com.senac.EasyCinemaWeb.entity.Proprietario;

public class ProprietarioDAO extends DAO<Proprietario> {

    public ProprietarioDAO() {
        super(Proprietario.class);
    }

}

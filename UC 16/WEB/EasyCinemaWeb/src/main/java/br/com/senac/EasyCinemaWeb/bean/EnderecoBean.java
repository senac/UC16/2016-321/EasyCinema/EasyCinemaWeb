package br.com.senac.EasyCinemaWeb.bean;

import br.com.senac.EasyCinemaWeb.banco.EnderecoDAO;
import br.com.senac.EasyCinemaWeb.entity.Endereco;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named(value = "enderecoBean")
@RequestScoped
public class EnderecoBean extends Bean {

    private Endereco endereco;
    private EnderecoDAO dao;

    public EnderecoBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new EnderecoDAO();
        this.novo();
    }

    public String getCodigo() {
        return this.endereco.getId() == 0 ? "" : String.valueOf(this.endereco.getId());
    }

    public void novo() {
        this.endereco = new Endereco();
    }

    public void salvar() {

        try {

            if (this.endereco.getId() == 0) {
                dao.save(endereco);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(endereco);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Endereco endereco) {
        try {
            dao.delete(endereco.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Endereco> getLista() {
        return this.dao.findAll();
    }

}



package br.com.senac.EasyCinemaWeb.bean;

import br.com.senac.EasyCinemaWeb.banco.ProprietarioDAO;
import br.com.senac.EasyCinemaWeb.entity.Proprietario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named(value = "proprietarioBean")
@RequestScoped
public class ProprietarioBean extends Bean {

    private Proprietario proprietario;
    private ProprietarioDAO dao;

    public ProprietarioBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new ProprietarioDAO();
        this.novo();
    }

    public String getCodigo() {
        return this.proprietario.getId() == 0 ? "" : String.valueOf(this.proprietario.getId());
    }

    public void novo() {
        this.proprietario = new Proprietario();
    }

    public void salvar() {

        try {

            if (this.proprietario.getId() == 0) {
                dao.save(proprietario);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(proprietario);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Proprietario endereco) {
        try {
            dao.delete(endereco.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Proprietario getEndereco() {
        return proprietario;
    }

    public void setEndereco(Proprietario endereco) {
        this.proprietario = endereco;
    }

    public List<Proprietario> getLista() {
        return this.dao.findAll();
    }

}

package br.com.senac.EasyCinemaWeb.banco;

import javax.persistence.EntityManager;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class JPAUtilTest {

    private EntityManager em;

    @Before
    public void init() {
        this.em = JPAUtil.getEntityManager();
    }

    @Test
    public void deveInstanciarEntityManager() {

        assertNotEquals(em, this);

    }

}

package br.com.senac.EasyCinemaWeb.entity;

import br.com.senac.EasyCinemaWeb.banco.FilmeDAO;
import java.util.List;

public class Teste {

    public static void main(String[] args) {

        Filme filme = new Filme();
        filme.setNome("It A Coisa");
        filme.setGenero("Terror");
        filme.setDescricao("Um Filme");
        filme.setClassificacao(18);
        filme.setDiretor("Eu");
        filme.setDuracao(90);
        filme.setElenco("Duas Mochilas");
        filme.setTresDimencoes("sim");

        FilmeDAO dao = new FilmeDAO();

        dao.save(filme);

        List<Filme> listaFilme = dao.findAll();

        for (Filme f : listaFilme) {
            System.out.println(f.getDescricao());

        }
    }

}

package br.com.senac.EasyCinemaWeb.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Filme implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false, length = 250)
    private String nome;

    @Column(nullable = false, length = 1000)
    private String descricao;

    @Column(nullable = false)
    private int duracao;

    @Column(nullable = false, length = 200)
    private String diretor;

    @Column(nullable = false, length = 1000)
    private String elenco;

    @Column(nullable = false)
    private int classificacao;

    @Column(nullable = false, length = 100)
    private String genero;

    @Column(nullable = false, length = 3)
    private String tresDimencoes;

    public Filme() {
    }

    public Filme(String nome, String descricao, int duracao, String diretor, String elenco, int classificacao, String genero, String tresDimencoes) {
        this.nome = nome;
        this.descricao = descricao;
        this.duracao = duracao;
        this.diretor = diretor;
        this.elenco = elenco;
        this.classificacao = classificacao;
        this.genero = genero;
        this.tresDimencoes = tresDimencoes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public String getElenco() {
        return elenco;
    }

    public void setElenco(String elenco) {
        this.elenco = elenco;
    }

    public int getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(int classificacao) {
        this.classificacao = classificacao;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTresDimencoes() {
        return tresDimencoes;
    }

    public void setTresDimencoes(String tresDimencoes) {
        this.tresDimencoes = tresDimencoes;
    }

}

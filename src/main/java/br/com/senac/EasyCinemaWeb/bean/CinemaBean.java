
package br.com.senac.EasyCinemaWeb.bean;


import br.com.senac.EasyCinemaWeb.banco.CinemaDAO;
import br.com.senac.EasyCinemaWeb.entity.Cinema;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value = "cinemaBean")
@RequestScoped
public class CinemaBean extends Bean{
    
    private Cinema cinema;
    private CinemaDAO dao;

    public CinemaBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new CinemaDAO();
        this.novo();
    }

    public String getCodigo() {
        return this.cinema.getId() == 0 ? "" : String.valueOf(this.cinema.getId());
    }

    public void novo() {
        this.cinema = new Cinema();
    }

    public void salvar() {

        try {

            if (this.cinema.getId() == 0) {
                dao.save(cinema);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(cinema);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Cinema cinema) {
        try {
            dao.delete(cinema.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public List<Cinema> getLista() {
        return this.dao.findAll();
    }

    
}

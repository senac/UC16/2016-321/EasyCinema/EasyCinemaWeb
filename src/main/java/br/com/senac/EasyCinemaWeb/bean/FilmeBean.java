package br.com.senac.EasyCinemaWeb.bean;

import br.com.senac.EasyCinemaWeb.banco.FilmeDAO;
import br.com.senac.EasyCinemaWeb.entity.Filme;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.util.List;
import javax.faces.view.ViewScoped;

@Named
@ViewScoped
public class FilmeBean extends Bean {

    private Filme filme;
    private FilmeDAO dao;

    public FilmeBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new FilmeDAO();
        this.novo();
    }

    public String getCodigo() {
        return this.filme.getId() == 0 ? "" : String.valueOf(this.filme.getId());
    }

    public void novo() {
        this.filme = new Filme();
        
    }

    public void salvar() {

        try {

            if (this.filme.getId() == 0) {
                dao.save(filme);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(filme);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Filme filme) {
        try {
            dao.delete(filme.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
    
    public void salvarEdicao() {

        try {

            if (this.filme.getId() == 0) {
                addMessageInfo("Não é possível cadastrar aqui!!");
            } else {
                dao.update(filme);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public Filme getFilme() {
        return filme;
    }

    public void setFilme(Filme filme) {
        this.filme = filme;
    }

    public List<Filme> getLista() {
        return this.dao.findAll();
    }
}

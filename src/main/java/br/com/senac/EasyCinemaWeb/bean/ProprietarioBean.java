package br.com.senac.EasyCinemaWeb.bean;

import br.com.senac.EasyCinemaWeb.banco.ProprietarioDAO;
import br.com.senac.EasyCinemaWeb.entity.Proprietario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FlowEvent;

@Named(value = "proprietarioBean")
@RequestScoped
public class ProprietarioBean extends Bean {

    private Proprietario proprietario;
    private ProprietarioDAO dao;
    private boolean skip;

    public ProprietarioBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new ProprietarioDAO();
        this.novo();
    }

    public String getCodigo() {
        return this.proprietario.getId() == 0 ? "" : String.valueOf(this.proprietario.getId());
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //Reseta caso o usuario clique em voltar.
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public void novo() {
        this.proprietario = new Proprietario();
    }

    public void salvar() {

        try {

            if (this.proprietario.getId() == 0) {
                dao.save(proprietario);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(proprietario);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Proprietario proprietario) {
        try {
            dao.delete(proprietario.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Proprietario getProprietario() {
        return proprietario;
    }

    public void setProprietario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }

    public List<Proprietario> getLista() {
        return this.dao.findAll();
    }

}

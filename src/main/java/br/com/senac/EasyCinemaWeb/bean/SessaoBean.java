
package br.com.senac.EasyCinemaWeb.bean;

import br.com.senac.EasyCinemaWeb.banco.SessaoDAO;
import br.com.senac.EasyCinemaWeb.entity.Sessao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;


@Named(value = "sessaoBean")
@RequestScoped
public class SessaoBean extends Bean{

    private Sessao sessao;
    private SessaoDAO dao;
    
    public SessaoBean() {
    }
    
    @PostConstruct
    public void init() {
        this.dao = new SessaoDAO();
        this.novo();
    }

    public String getCodigo() {
        return this.sessao.getId() == 0 ? "" : String.valueOf(this.sessao.getId());
    }

    public void novo() {
        this.sessao = new Sessao();
    }

    public void salvar() {

        try {

            if (this.sessao.getId() == 0) {
                dao.save(sessao);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(sessao);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Sessao sessao) {
        try {
            dao.delete(sessao.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Sessao getSessao() {
        return sessao;
    }

    public void setSessao(Sessao sessao) {
        this.sessao = sessao;
    }

    public List<Sessao> getLista() {
        return this.dao.findAll();
    }
    
}

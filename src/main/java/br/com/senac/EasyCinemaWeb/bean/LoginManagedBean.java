
package br.com.senac.EasyCinemaWeb.bean;
   
import br.com.senac.EasyCinemaWeb.banco.ProprietarioDAO;
import br.com.senac.EasyCinemaWeb.banco.UsuarioDAO;
import br.com.senac.EasyCinemaWeb.entity.Proprietario;
import br.com.senac.EasyCinemaWeb.entity.Usuario;

  import javax.faces.application.FacesMessage;
  import javax.faces.bean.ManagedBean;
  import javax.faces.bean.ViewScoped;
  import javax.faces.context.FacesContext;

import javax.persistence.NoResultException;
   
  @ManagedBean(name = "LoginMB")
  @ViewScoped
  public class LoginManagedBean {
   
        private ProprietarioDAO proprietarioDAO = new ProprietarioDAO();
        private Proprietario proprietario = new Proprietario();
        
        public String envia() {
              
              proprietario = proprietarioDAO.getProprietario(proprietario.getNome(), proprietario.getCpf());
              if (proprietario == null) {
                    proprietario = new Proprietario();
                    FacesContext.getCurrentInstance().addMessage(
                               null,
                               new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!",
                                           "Erro no Login!"));
                    return "../filmes/gerenciar.xhtml";
              } else {
                    return "../filmes/gerenciar.xhtml" ; //colocar a página para a qual deve retornar!! 
              }
              
              
        }
   
        public Proprietario getProprietario() {
              return proprietario;
        }
   
        public void setProprietario(Proprietario proprietario) {
              this.proprietario = proprietario;
        }
        
  }
package br.com.senac.EasyCinemaWeb.banco;

import br.com.senac.EasyCinemaWeb.entity.Filme;

public class FilmeDAO extends DAO<Filme> {

    public FilmeDAO() {
        super(Filme.class);
    }
}

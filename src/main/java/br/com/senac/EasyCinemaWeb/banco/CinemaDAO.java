
package br.com.senac.EasyCinemaWeb.banco;

import br.com.senac.EasyCinemaWeb.entity.Cinema;


public class CinemaDAO extends DAO<Cinema>{
    
    public CinemaDAO() {
        super(Cinema.class);
    }
    
}

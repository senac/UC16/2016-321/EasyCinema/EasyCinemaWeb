package br.com.senac.EasyCinemaWeb.banco;

import br.com.senac.EasyCinemaWeb.entity.Sessao;

public class SessaoDAO extends DAO<Sessao> {

    public SessaoDAO() {
        super(Sessao.class);
    }

}

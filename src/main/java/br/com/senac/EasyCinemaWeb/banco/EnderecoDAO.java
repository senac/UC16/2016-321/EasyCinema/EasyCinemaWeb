package br.com.senac.EasyCinemaWeb.banco;

import br.com.senac.EasyCinemaWeb.entity.Endereco;

public class EnderecoDAO extends DAO<Endereco> {

    public EnderecoDAO() {
        super(Endereco.class);
    }

}

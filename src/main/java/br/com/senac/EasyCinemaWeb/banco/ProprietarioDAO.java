package br.com.senac.EasyCinemaWeb.banco;

import br.com.senac.EasyCinemaWeb.entity.Proprietario;
import br.com.senac.EasyCinemaWeb.entity.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

public class ProprietarioDAO extends DAO<Proprietario> {

    public ProprietarioDAO() {
        super(Proprietario.class);
    }
    
    private EntityManagerFactory factory = Persistence
                    .createEntityManagerFactory("EasyCinema");
        private EntityManager em = factory.createEntityManager();
   
        public Proprietario getProprietario(String nome, int cpf ) {
   
              try {
                    Proprietario proprietario = (Proprietario) em
                               .createQuery(
                                           "SELECT p from Proprietario p where p.nome = :nome and p.cpf = :cpf")
                               .setParameter("name", nome)
                               .setParameter("senha", cpf).getSingleResult();
   
                    return proprietario;
              } catch (NoResultException e) {
                    return null;
              }
        }
   
      public boolean inserirProprietario(Proprietario proprietario) {
              try {
                    em.persist(proprietario);
                    return true;
              } catch (Exception e) {
                    e.printStackTrace();
                    return false;
              }
        }
        
        public boolean deletarProprietario(Proprietario proprietario) {
              try {
                    em.remove(proprietario);
                    return true;
              } catch (Exception e) {
                    e.printStackTrace();
                    return false;
              }
        }

    
}

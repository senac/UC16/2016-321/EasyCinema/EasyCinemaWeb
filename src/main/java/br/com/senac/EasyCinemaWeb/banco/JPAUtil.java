package br.com.senac.EasyCinemaWeb.banco;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("EasyCinema");

    public static EntityManager getEntityManager() {

        try {
            return emf.createEntityManager();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Falha ao acessar o banco.");
        }

    }
    
    public static void main(String... args ){
        
        JPAUtil.getEntityManager() ; 
        
        
    }
    

}

